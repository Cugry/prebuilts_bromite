# Bromite - Take back your browser

![](https://www.bromite.org/android-icon-192x192.png)

> Bromite is a [Chromium](https://www.chromium.org/Home) fork with support for ad blocking and enhanced privacy.  
> Bromite is only available for Android Lollipop (v5.0, API level 21) and above.  
> 
> For documentation see the [wiki](https://github.com/bromite/bromite/wiki).

This repository provides prebuilt apk (from latest release) for custom ROMs.

## How to use

add this to manifest or local_manifests

```xml
<project path="prebuilts/bromite" name="Cugry/prebuilts_bromite" remote="gitlab" revision="master" clone-depth="1" />
```

add this to vendor/lineage or vendor/extra
```makefile
PRODUCT_PACKAGES += \
    Bromite
```
